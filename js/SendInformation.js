
function AjaxRequest(method, address, handler, informations)
{
    method = method.toUpperCase();

    var Request = {
        type: method,
        url: address,
        success: function (callback) {
            window[handler](callback);
        }
    };

    if (typeof informations !== undefined)
    {
        Request['data'] = informations;
    }

    $.ajax(Request);

}


ContactUs = {
    name_surname : $('#name_surname').val(),
    email : $('#email').val(),
    phone : $('#phone').val(),
    
};